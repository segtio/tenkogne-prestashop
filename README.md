# Prestashop Developpement de Module
## Intro
Ce workshop explique comment développer différents modules pour PrestaShop 1.6.
Nous verrons à travers les meilleures pratiques largement utilisés et 
vous donner les moyens d'utiliser ces méthodes tout en développant diverses fonctions au sein de PrestaShop.

Nous Aborderons les notions suivantes : 
 - HOOKS
 - Context
 - Controllers, Objects Models et Overrides
 
## Cas Pratique
 Nous allons développer un module qui permettra aux clients de noter et de commenter les produits.
 
### Partie 1
Dans ce premier chapitre, nous allons voir comment:
 - Créez le Bootstrap d'un module installable
 - Ajouter un formulaire de configuration pour le module en utilisant des modèles Smarty
 - Enregistrez la configuration du module dans une base de données
 
#### Structure de la Class Module
##### Le contructeur
Afin de faire fonctionner le module , nous avons juste à ajouter la méthode constructeur.
Dans cette méthode, vous devez écrire les trois lignes obligatoires suivantes:
 - le nom technique : $this->name [Cette variable est utilisée par PrestaShop effectuer l'installation, la désinstallation et configurer les liens du module]
 - le nom public : $this->displayName [Cette ligne est utilisée pour afficher le nom du module à l'utilisateur dans la liste des modules du back office]
 - le contructeur Parent : parent::__construct();

Vous pouvez ajouter quelques-unes des lignes optionnelles suivantes pour donner des informations sur le module:
 - la categorie: $this->tab
 - l'auteur : $this->author
 - la version : $this->version
 - la description : $this->description
#### La Configuration
Prestashop alloue la table "ps_configuration" aux configuration des modules.
Nous allons maintenant ajouter des options de configuration à notre module. 
Pour ce faire, il suffit d'ajouter la fonction getContent à notre module. 
La valeur de retour de cette fonction sera le contenu affiché sur votre écran (de l'HTML).

	public function getContent()
	{
		return < h1 > hello </ h1 >;
	}
#### Le Formulaire
Nous allons utiliser le framework Boostrap, et pour charder ses composants nous allons simplement rajouter dans notre contructeur ( $this->bootstrap = true;)

Afin de ne pas surcharger la méthode getContent, nous allons créer une nouvelle méthode (processConfiguration)
dédié à la manipulation de la soumission du formulaire de configuration du module.

Nous allons aussi creer (assignConfiguration) qui nous permettra de charger la configuration enregistrée

	public function processConfiguration()
	{
		if (Tools::isSubmit('mymod_pc_form'))
		{
			$enable_rates = Tools::getValue('enable_rates');
			$enable_comments = Tools::getValue('enable_comments');
			Configuration::updateValue('MYMOD_RATES', $enable_rates);
			Configuration::updateValue('MYMOD_COMMENTS', $enable_comments);
			$this->context->smarty->assign('confirmation', 'ok');
		}
	}

    public function assignConfiguration()
        {
            $enable_rates = Configuration::get('MYMOD_RATES');
            $enable_comments = Configuration::get('MYMOD_COMMENTS');
            $this->context->smarty->assign('enable_rates', $enable_rates);
            $this->context->smarty->assign('enable_comments', $enable_comments);
        }

	public function getContent()
	{
		$this->processConfiguration();
		$this->assignConfiguration();
		return $this->display(__FILE__, 'getContent.tpl');
	}
### Partie 2 
Un HOOK est le concept le plus essentiel à comprendre si vous voulez coder un module de PrestaShop.
Les HOOKS sont les points sur lesquels vous pouvez joindre les modules de facon à changer le comportement normal de la boutique. 
Dans le code source PrestaShop 1.5 / 1.6, leurs noms sont généralement préfixées avec display ou action, en fonction de leur role.
##### La liste de HOOKS pour prestashop 1.6
 http://www.team-ever.com/prestashop-1-6-la-liste-des-hooks/
Nous allons donc voir Comment:
 - Enregistrer un module sur un HOOK (pour afficher les commentaires sur le front office)
 - Utilisez une classe de base de données (pour enregistrer les commentaires des utilisateurs dans la base de données)
 - Les HOOKS sont déclenchés
 - Ajouter de nouveaux HOOKS
 - Utiliser des hooks dynamiques
 
#### Accrocher un module aux HOOKS
Nous allons réécrire la fonction install() qui s'execute à l'installation du module et y rajouter notre HOOK comme suit :

    public function install()
    {
    parent::install();
    $this->registerHook('displayProductTabContent');
    return true;
    }

Ensuite, nous aurons besoin d'écrire une fonction dans votre module nommé hook{nomduhook}.
Dans notre cas, il sera hookTabContentDisplayProduct (un hook de type affichage).
Nous aurons besion, comme pour la configuration, d'avoir les fonctions processProductTabContent() et assignProductTabContent() pour enregistrer et charger les commentaires


    public function processProductTabContent()
        {
            if (Tools::isSubmit('mymod_pc_submit_comment'))
            {
                $id_product = Tools::getValue('id_product');
                $rate = Tools::getValue('rate');
                $comment = Tools::getValue('comment');
                $insert = array(
                    'id_product' => (int)$id_product,
                    'rate' => (int)$rate,
                    'comment' => pSQL($comment),
                    'date_add' => date('Y-m-d H:i:s'),
                );
                Db::getInstance()->insert('mymod_comment', $insert);
            }
        }

    public function assignProductTabContent()
    {
        $enable_rates = Configuration::get('MYMOD_RATES');
        $enable_comments = Configuration::get('MYMOD_COMMENTS');

        $id_product = Tools::getValue('id_product');
        $comments = Db::getInstance()->executeS('
		SELECT * FROM `'._DB_PREFIX_.'mymod_comment`
		WHERE `id_product` = '.(int)$id_product);

        $this->context->smarty->assign('enable_rates', $enable_rates);
        $this->context->smarty->assign('enable_comments', $enable_comments);
        $this->context->smarty->assign('comments', $comments);
    }

     public function hookDisplayProductTabContent($params)
        {
            $this->processProductTabContent();
            $this->assignProductTabContent();
            return $this->display(__FILE__, 'displayProductTabContent.tpl');
        }
        
Maintenant que nous avons notre mechanisme en place, créons notre table ps_mymod_comment

    CREATE TABLE IF NOT EXISTS ps_mymod_comment (id_mymod_comment
    int(11) NOT NULL AUTO_INCREMENT,
    id_product int(11) NOT NULL,
    rate tinyint(1) NOT NULL,
    comment text NOT NULL,date_add datetime NOT NULL,
    PRIMARY KEY (id_mymod_comment)) ENGINE=InnoDB
    DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
    
#### Declancher les HOOKS
Dans le code source PrestaShop, vous trouverez deux types de déclencheurs de Hook:
 - dans les fichier .tpl : {hook h='hookName'}
 - dans les fichiers .php : Hook::exec('hookName')
Dans ce cas , vous trouverez dans le fichier /classes/controllers/ProductController.php : Hook::exec('displayProductTabContent');

#### Creer son propre hook
Vous aurez certainement besoin d'avoir votre proper hook. 
Et pour se faire ,il vous suffit de placer le trigger de votre hook dans le fichier de fichier PHP ou le tpl que vous voulez et
fixer le module au nouveau hook avec la méthode registerHook.
registerHook se chargera de l'inserer dans la base de donnée si il n'existe pas encore.

#### Hook Dynamique
vous constaterez que dans une partie du CMS, il y a des hooks dont les noms sont construits de façon dynamique. 
Vous les trouverez dans les classes abstraites telles que ObjectModel et AdminController, ou admin helper modèles tels que form.tpl et view.tpl.
En prenant l'exemple de /classes/ObjectModel.php, nous avons :
    
    Hook::exec('actionObjectAddBefore', array('object' => $this));
    Hook::exec('actionObject'.get_class($this).'AddBefore', array
    ('object' => $this));
    
ce hook aura un nom différent pour chaque classe ObjectModel; vous serez en mesure d'enregistrer votre module sur un action spécifique d'une classe ObjectModel spécifique.
Dans ce cas, les hooks dynamiques sont placés avant et après chaque ajout principal, mise à jour, et supprimer l'action, ce qui vous donne beaucoup de possibilités.

### Partie 3
Dans PrestaShop, l'objet de contexte contient des objets communs (tels que les cookies, le langage courant, etc.) et des services (tels que Smarty).

Dans cette partie, nous allons apprendre à :
 - Utilisez la méthode l et traduire le  module en francais
 - Ajouter du CSS  et ajouter des méthodes JS pour améliorer l'ergonomie
 
#### Traduction du module
Le premier est la methode l. Cette méthode est utilisée pour traduire le texte. 
Nous traduirons le displayName et la Description :

       $this->displayName = $this->l('My Module of product comments');
       $this->description = $this->l('With this module, your customers
       will be able to grade and comments your products.');

Nous irons ensuite dans {module} > configuration > traduire > {francais} pour charger le text  traduit

Dans les templates nous utiliserons la meme methode mais sous une autre forme

    <h3 class="page-product-heading" id="mymodcomments-content-tab"{if isset($new_comment_posted)} data-scroll="true"{/if}>{l s='Product Comments' mod='rateandcomment'}</h3>

#### Ajourter du CSS et du JS
Nous allons maintenant examiner deux nouvelles fonctions, qui vous permettra d'ajouter CSS et JS dans
votre module:

    $this->context->controller->addCSS($this->_path.'views/css/mymodcomments.css', 'all');
    $this->context->controller->addJS($this->_path.'views/js/mymodcomments.js');
    
Ces methodes sont inserées par ce bout de code disposé dans le header.tpl du theme (les fichiers JS sont dans le footer)

    {if isset($css_files)}
    {foreach from=$css_files key=css_uri item=media}
    <link rel="stylesheet" href="{$css_uri}" type="text/css"
    media="{$media}" />
    {/foreach}
    {/if}

### Partie 4
vous conviendrez que ce n'est pas très pratique pour l'utilisateur qui veut installer le module d'executer des requete SQL comme nous l'avons fait plutot. 
PETIT PROBLEME : Il n'y a pas de méthodes PrestaShop officielles pour créer, supprimer, ou mettre à jour une table de base de données dans un module. 
Je vais vous montrer celui utilisé dans la plupart des modules de PrestaShop natifs.
     
Dans cette partie, nous allons voir comment faire pour:
 - Créer une table SQL lorsque le module est installé
 - Supprimer une table SQL lorsque le module est désinstallé
 - Modifier la table SQL existante lorsque le module est mis à jour
 
#### Créer une table SQL lorsque le module est installé
Créons notre fichier install.sql
    
    CREATE TABLE IF NOT EXISTS `PREFIX_mymod_comment` (
      `id_mymod_comment` int(11) NOT NULL AUTO_INCREMENT,
      `id_product` int(11) NOT NULL,
      `firstname` VARCHAR( 255 ) NOT NULL,
      `lastname` VARCHAR( 255 ) NOT NULL,
      `email` VARCHAR( 255 ) NOT NULL,
      `grade` tinyint(1) NOT NULL,
      `comment` text NOT NULL,
      `date_add` datetime NOT NULL,
      PRIMARY KEY (`id_mymod_comment`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

Nous pourrions avoir à parser le fichier SQL pour la désinstallation et la mise à jour des Modules ,
donc créer une méthode pour cela serai plus adequat.

    public function loadSQLFile($sql_file)
    {
        $sql_content = file_get_contents($sql_file);

        // Replace prefix and store SQL command in array
        $sql_content = str_replace('PREFIX_', _DB_PREFIX_, $sql_content);
        $sql_requests = preg_split("/;\s*[\r\n]+/", $sql_content);
        
        $result = true;
        foreach($sql_requests as $request)
            if (!empty($request))
                $result &= Db::getInstance()->execute(trim($request));
        
        return $result;
    }

Maintenant, nous avons juste à appeler la méthode dans la méthode d'installation juste après l'appel de parent::install()

    public function install()
    {
        if (!parent::install())
            return false;

        $sql_file = dirname(__FILE__).'/install/install.sql';
        if (!$this->loadSQLFile($sql_file))
            return false;

        if (!$this->registerHook('displayProductTabContent') ||
            !$this->registerHook('displayBackOfficeHeader'))
            return false;

        Configuration::updateValue('MYMOD_RATES', '1');
        Configuration::updateValue('MYMOD_COMMENTS', '1');

        return true;
    }
    
#### Supprimer une table SQL lorsque le module est désinstallé
Nous aurons aussi besoin d'un uninstall.sql
    
    DROP TABLE `PREFIX_mymod_comment`;

Puis appelons le dans la fonction uninstall du module

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        
        $sql_file = dirname(__FILE__).'/install/uninstall.sql';
        if (!$this->loadSQLFile($sql_file))
            return false;
        
        Configuration::deleteByName('MYMOD_RATES');
        Configuration::deleteByName('MYMOD_COMMENTS');
        
        return true;
    }

#### Modifier la table SQL existante lorsque le module est mis à jour
Cette section est seulement pour les versions PrestaShop 1.5  et plus récentes

Ajoutons quelques champs (nom, prénom, adresse et e-mail) au commentaireformer sur les pages produits. 
Nous allons non seulement mettre à jour le PHP et le modèle fichiers, mais aussi modifier la table mymod_comment SQL

Tout d'abord, dans mon rateandcomment.php, changeons la version de notre module, qui était 0.1  à 0.2 dans notre constructeur.
Créons ensuite  le fichier upgrade/sql/install-{module_version}.sql, dans notre cas upgrade/sql/install-0.2.sql.

    ALTER TABLE `PREFIX_mymod_comment`
    ADD `firstname` VARCHAR( 255 ) NOT NULL AFTER `id_product` ,
    ADD `lastname` VARCHAR( 255 ) NOT NULL AFTER `firstname` ,
    ADD `email` VARCHAR( 255 ) NOT NULL AFTER `lastname`
    
Ensuite notre  upgrade/install-{module_version}.php qui chargera ce fichier

    function upgrade_module_0_2($module)
    {
    	$sql_file = dirname(__FILE__).'/sql/install-0.2.sql';
    	if (!$module->loadSQLFile($sql_file))
    		return false;
    	return true;
    }

#### Ajout d'une fonction de callback aux options
Une dernière méthode que je veux vous présenter onClickOption. 
Cette méthode vous permettre de définir le déclencheur JavaScript OnClick sur les boutons d'action du module.

    public function onClickOption($type, $href = false)
        {
            $confirm_reset = $this->l('Reseting this module will delete all comments from your database, are you sure you want to reset it ?');
            $reset_callback = "return mymodcomments_reset('".addslashes($confirm_reset)."');";
    
            $matchType = array(
                'reset' => $reset_callback,
                'delete' => "return confirm('Confirm delete?')",
            );
    
            if (isset($matchType[$type]))
                return $matchType[$type];
    
            return '';
        }