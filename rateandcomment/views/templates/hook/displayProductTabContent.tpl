<h3 class="page-product-heading" id="mymodcomments-content-tab"{if isset($new_comment_posted)} data-scroll="true"{/if}>{l s='Product Comments' mod='rateandcomment'}</h3>

<div class="rte">
    {foreach from=$comments item=comment}
        <div class="mymodcomments-comment">
            <img src="http://www.gravatar.com/avatar/{$comment.email|trim|strtolower|md5}?s=45" class="pull-left img-thumbnail mymodcomments-avatar" />
            <p>{$comment.firstname} {$comment.lastname|substr:0:1}.</p>
            <div class="star-rating"><i class="glyphicon glyphicon-star"></i> <strong>{l s='Rate:' mod='rateandcomment'}</strong></div> <input value="{$comment.rate}" type="number" class="rating" min="0" max="5" step="1" data-size="xs" />
            <div><i class="glyphicon glyphicon-comment"></i> <strong>{l s='Comment' mod='rateandcomment'} #{$comment.id_mymod_comment}:</strong> {$comment.comment}</div>
        </div>
        <hr />
    {/foreach}
</div>

{if $enable_rates eq 1 OR $enable_comments eq 1}
    <div class="rte">
        <form action="" method="POST" id="comment-form">

            <div class="form-group">
                <label for="firstname">{l s='Firstname:' mod='rateandcomment'}</label>
                <div class="row"><div class="col-xs-4">
                        <input type=”text” name="firstname" id="firstname" class="form-control" />
                    </div></div>
            </div>
            <div class="form-group">
                <label for="lastname">{l s='Lastname:' mod='rateandcomment'}</label>
                <div class="row"><div class="col-xs-4">
                        <input type=”text” name="lastname" id="lastname" class="form-control" />
                    </div></div>
            </div>
            <div class="form-group">
                <label for="email">{l s='Email:' mod='rateandcomment'}</label>
                <div class="row"><div class="col-xs-4">
                        <input type=”email” name="email" id="email" class="form-control" />
                    </div></div>
            </div>

            {if $enable_rates eq 1}
                <div class="form-group">
                    <label for="rate">{l s='Rate:' mod='rateandcomment'}</label>
                    <div class="row">
                        <div class="col-xs-4" id="rate-tab">
                            <input id="rate" name="rate" value="0" type="number" class="rating" min="0" max="5" step="1" data-size="sm" >
                        </div>
                    </div>
                </div>
            {/if}
            {if $enable_comments eq 1}
                <div class="form-group">
                    <label for="comment">{l s='Comment:' mod='rateandcomment'}</label>
                    <textarea name="comment" id="comment" class="form-control"></textarea>
                </div>
            {/if}
            <div class="submit">
                <button type="submit" name="mymod_pc_submit_comment" class="button btn btn-default button-medium"><span>{l s='Send' mod='rateandcomment'}<i class="icon-chevron-right right"></i></span></button>
            </div>
        </form>
    </div>
{/if}